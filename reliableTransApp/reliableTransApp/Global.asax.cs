﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Globalization;
using System.Threading;

namespace reliableTransApp {
	public class MvcApplication : System.Web.HttpApplication {

		protected void Application_Start()
		{
			//Изменение текущей культуры, в соответствии в выбранной в выпадающем списке

			string culture = "ru-RU";
			CultureInfo ci = new CultureInfo(culture);

			Thread.CurrentThread.CurrentUICulture = ci;
			Thread.CurrentThread.CurrentCulture = ci;




			AreaRegistration.RegisterAllAreas();

			WebApiConfig.Register(GlobalConfiguration.Configuration);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);

			AuthConfig.RegisterAuth();

			ValueProviderFactories.Factories.Add(new JsonValueProviderFactory());
		}

		protected void Application_AcquireRequestState(object sender, EventArgs e)
		{
			//Устанавливаем текущий формат отображения данных в соответствии с правилами россии
			CultureInfo ci = new CultureInfo("ru-RU");
			Thread.CurrentThread.CurrentUICulture = ci;
			Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
		}
	}
}