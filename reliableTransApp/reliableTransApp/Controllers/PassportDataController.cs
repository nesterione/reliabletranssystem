﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using reliableTransApp.Models.DataBaseContext;
using reliableTransApp.Models;

namespace reliableTransApp.Controllers {
	public class PassportDataController : Controller {


		[HttpPost]
		public string AddPassData(PassportData passport)
		{
			Guid IdNewPass = passport.Id;

			if (passport != null)
			{
				PassportDataModel passportDataModel = new PassportDataModel();
				passportDataModel.Create(passport);
			}

			return passport.Id.ToString();
		}
	}
}
