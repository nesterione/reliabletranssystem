﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using reliableTransApp.Models.DataBaseContext;
using reliableTransApp.Models;

namespace reliableTransApp.Controllers
{
	[Authorize]
    public class AddressController : Controller
    {
		 [HttpPost]
		 public string AddAdress(Address address)
		 {
			 Guid IdNewAddress = address.Id;
			 if (address != null)
			 {
				 AddressModel addressModel = new AddressModel();
				 addressModel.Create(address);
		
			 }
			 return address.Id.ToString();
		 }
    }
}
