﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using reliableTransApp.Models;
using reliableTransApp.Models.DataBaseContext;
using Newtonsoft.Json;

namespace reliableTransApp.Controllers {
	/// <summary>
	/// класс: TownController
	/// 
	/// автор: Игорь Нестереня
	/// дата: 07 сентября 2013 года
	/// 
	/// Контроллер для управления городами назначения
	/// </summary>
	[Authorize]
	public class TownController : Controller {
		// GET: /Town/

		TownModel townModel = new TownModel();

		public ActionResult Index()
		{
			return View(townModel.GetAll());
		}

    [HttpPost]
    public String GetTowns(string query) {
      List<Town> towns = new List<Town>();
      if (query != null) {
        TownModel townModel = new TownModel();
        towns = townModel.GetFind(query, 5);
      }
      return JsonConvert.SerializeObject(towns);
    }

		//
		// GET: /Town/Details

		public ActionResult Details(Guid id)
		{
			Town town = townModel.Get(id);
			if (town == null)
			{
				return HttpNotFound();
			}
			return View(town);
		}

		//
		// GET: /Town/Create

		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Town/Create

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(Town town)
		{

			if (ModelState.IsValid)
			{
				townModel.Create(town);
				return RedirectToAction("Index");
			}

			return View(town);
		}

		[HttpPost]
		public String CreateAJAX(Town town)
		{
			townModel.Create(town);
			return JsonConvert.SerializeObject(town);
		}
		//
		// GET: /Town/Edit/5

		public ActionResult Edit(Guid id)
		{
			Town town = townModel.Get(id);
			if (town == null)
			{
				return HttpNotFound();
			}
			return View(town);
		}

		//
		// POST: /Town/Edit/5

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(Town town)
		{
			if (ModelState.IsValid)
			{
				townModel.Update(town);
				return RedirectToAction("Index");
			}
			return View(town);
		}

		//
		// GET: /Town/Delete/5

		public ActionResult Delete(Guid id)
		{
			Town town = townModel.Get(id);
			if (town == null)
			{
				return HttpNotFound();
			}
			return View(town);
		}

		//
		// POST: /Town/Delete/5

		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(Guid id)
		{
			townModel.Delete(id);
			return RedirectToAction("Index");
		}

	}
}
// (с) Copyright 1991-2013 Igor Nesterenya. All Rights Reserved.