﻿using Newtonsoft.Json;
using reliableTransApp.Models;
using reliableTransApp.Models.DataBaseContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace reliableTransApp.Controllers
{
    public class JournalController : Controller
    {
        //
        // GET: /Journal/

        public ActionResult Index()
        {
            return View();
        }

		  public ActionResult ViewJournalRB()
		  {
          //Переча в ViewModel информации о Городах
          TownModel townModel = new TownModel();
          var allTowns = townModel.GetAll();
          ViewBag.Towns = allTowns;



          JornalRBModel jornalModel = new JornalRBModel();
          List<Models.DataBaseContext.ExpressRB> expressRBActual = jornalModel.GetActual();

          Dictionary<DateTime, List<ExpressRB>> dictionaryRB = new Dictionary<DateTime, List<ExpressRB>>();
          foreach (ExpressRB expressRB in expressRBActual) {
            if (expressRB.DateUnloading == null)
              continue;

            //Поскольку дата проверена на null приведеме её
            DateTime date = (DateTime)expressRB.DateUnloading;

            if (dictionaryRB.ContainsKey(date)) {
              dictionaryRB[date].Add(expressRB);   //.DateUnloading, expressRB);
            } else {
              dictionaryRB.Add(date, new List<ExpressRB>() { expressRB });
            }
          }

          ViewBag.CollectionExpressRBSortedByDate = dictionaryRB.Values;

          return View();
		  }

		  public ActionResult ViewJournalRF()
		  {
        throw new NotImplementedException();
		  }

		  public ActionResult ViewJournalExpedition()
		  {
        throw new NotImplementedException();
		  }

      public ActionResult Summary() {
        //Переча в ViewModel информации о Городах
        TownModel townModel = new TownModel();
        var allTowns = townModel.GetAll();
        ViewBag.Towns = allTowns;



        JornalRBModel jornalModel = new JornalRBModel();
        List<Models.DataBaseContext.ExpressRB> expressRBActual = jornalModel.GetAll();

        Dictionary<DateTime, List<ExpressRB>> dictionaryRB = new Dictionary<DateTime, List<ExpressRB>>();
        foreach (ExpressRB expressRB in expressRBActual) {
          if (expressRB.DateUnloading == null)
            continue;

          //Поскольку дата проверена на null приведеме её
          DateTime date = (DateTime)expressRB.DateUnloading;

          if (dictionaryRB.ContainsKey(date)) {
            dictionaryRB[date].Add(expressRB);   //.DateUnloading, expressRB);
          } else {
            dictionaryRB.Add(date, new List<ExpressRB>() { expressRB });
          }
        }

        ViewBag.CollectionExpressRBSortedByDate = dictionaryRB.Values;

        return View("ViewJournalRB");
      }

      public class EnumValue {
        public int Value { get; set; }
        public Enum NameEnum { get; set; }
        public String Description { get; set; }

        public EnumValue(int Value, Enum NameEnum, String Description) {
          this.Value = Value;
          this.NameEnum = NameEnum;
          this.Description = Description;
        }
      }

      public ActionResult CreateExpressRB() {

        //var ff = Status.Delivered.GetType().GetField(Status.Delivered.ToString());
        //DescriptionAttribute[] att = (DescriptionAttribute[])ff.GetCustomAttributes(typeof(DescriptionAttribute),false);

        ViewBag.Statuses = formStatusList();
        return View();
      }

      private List<EnumValue> formStatusList() {
        List<EnumValue> statuses = new List<EnumValue>();
        foreach (Status status in Enum.GetValues(typeof(Status))) {
          statuses.Add(new EnumValue((int)status, status, EnumHelper.getDescriptionAttr(status)));
        }
        return statuses;
      }

      //
      // POST: /Town/Create

      [HttpPost]
      [ValidateAntiForgeryToken]
      public void CreateExpressRB(ExpressRB expressRB, List<Cargo> Cargoes) {
        if (expressRB != null) {
          JornalRBModel journalModel = new JornalRBModel();

          for (int i = 0; i < Cargoes.Count; i++)
            Cargoes[i].ExpressRBId = expressRB.Id;

          expressRB.Cargos = Cargoes;
          journalModel.Create(expressRB);
        }

        Response.Redirect("~/Journal/ViewJournalRB");
      }


      //TODO: в другой контроллер
      [HttpPost]
      public string AddContact(Contact contact) {
        if (contact != null) {
          ContactModel contactModel = new ContactModel();
          contactModel.Create(contact);
        } else {
          throw new Exception("Id недолжен быть null");
        }

        return contact.Id.ToString();
      }

     



      //
      // GET: /Town/Edit/5

      public ActionResult EditExpressRB(Guid id) {
        JornalRBModel jornalRBModel = new JornalRBModel();
        ExpressRB expressRB = jornalRBModel.Get(id);
        if (expressRB == null) {
          return HttpNotFound();
        }
        ViewBag.Statuses = formStatusList();
        return View(expressRB);
      }

      //
      // POST: /Town/Edit/5

      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult EditExpressRB(ExpressRB expressRB) {
        JornalRBModel jornalRBModel = new JornalRBModel();

        if (ModelState.IsValid) {
          jornalRBModel.Update(expressRB);
          return RedirectToAction("/ViewJournalRB");
        }


        return View();
      }
    }
}
