﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using reliableTransApp.Models.DataBaseContext;
using reliableTransApp.Models;
using Newtonsoft.Json;

namespace reliableTransApp.Controllers {
	[Authorize]
	public class CustomerController : Controller {
		//
		// GET: /Customer/

		public ActionResult Index()
		{
			CustomerModel model = new CustomerModel();
			List<Customer> customers = model.GetAll();
			 
			return View(customers);
		}

		private class CustomerView {
			public Guid Id { get; set; }
			public String Description { get; set; }
			public String Name { get; set; }

			public CustomerView(Guid Id, String Name, String Description)
			{
				this.Id = Id;
				this.Name = Name;
				this.Description = Description;
			}
		}

		[HttpPost]
		public String GetCustomers(string query)
		{
			List<Customer> customers = null;
			if (query != null)
			{
				CustomerModel customerModel = new CustomerModel();
				customers = customerModel.Find(query,7);
			}

			List<CustomerView> customersView = new List<CustomerView>();
			foreach (Customer customer in customers)
				customersView.Add(new CustomerView(customer.Id, customer.GetName(), customer.ToString().Replace("\n", "<br/>")));

			return JsonConvert.SerializeObject(customersView);
		}

		[HttpPost]
		public bool Delete(Guid customerID)
		{
			bool rezult = false;

			try
			{
				if (customerID != default(Guid))
				{
					CustomerIRBModel customerIRBModel = new CustomerIRBModel();
					customerIRBModel.Delete(customerID);
					rezult = true;
				}
			}
			catch { };

			return rezult;
		}

		[HttpPost]
		public string AddNewCustomerIRB(CustomerIRB customerIRB)
		{
			if (customerIRB != null)
			{
				CustomerIRBModel customerIRBModel = new CustomerIRBModel();
				customerIRBModel.Create(customerIRB);
			}
			else
			{
				throw new Exception("Id недолжен быть null");
			}

			return JsonConvert.SerializeObject(new CustomerView(
				customerIRB.Id,
				customerIRB.GetName(),
				""));
		}
	}
}