﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using reliableTransApp.Models.DataBaseContext;

namespace reliableTransApp.Controllers
{
    public class TariffsController : Controller
    {
        private RTransContext db = new RTransContext();

        //
        // GET: /Tariffs/

        public ActionResult Index()
        {
            return View(db.Tariffs.ToList());
        }

        //
        // GET: /Tariffs/Details/5

        public ActionResult Details(Guid id = default(Guid))
        {
            Tariff tariff = db.Tariffs.Find(id);
            if (tariff == null)
            {
                return HttpNotFound();
            }
            return View(tariff);
        }

        //
        // GET: /Tariffs/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Tariffs/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tariff tariff)
        {
            if (ModelState.IsValid)
            {
                tariff.Id = Guid.NewGuid();
                db.Tariffs.Add(tariff);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tariff);
        }

        //
        // GET: /Tariffs/Edit/5

        public ActionResult Edit(Guid id = default(Guid))
        {
            Tariff tariff = db.Tariffs.Find(id);
            if (tariff == null)
            {
                return HttpNotFound();
            }
            return View(tariff);
        }

        //
        // POST: /Tariffs/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tariff tariff)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tariff).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tariff);
        }


        //
        // GET: /Tariffs/Delete/5

        public ActionResult Delete(Guid id = default(Guid))
        {
            Tariff tariff = db.Tariffs.Find(id);
            if (tariff == null)
            {
                return HttpNotFound();
            }
            return View(tariff);
        }

        //
        // POST: /Tariffs/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Tariff tariff = db.Tariffs.Find(id);
            db.Tariffs.Remove(tariff);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [HttpPost]
        public double GetStandartRate() {

         
            Tariff tariff = db.Tariffs.Where( curTariff => curTariff.Name.Equals("Основной")).First();
            if (tariff == null) {
              throw new Exception("Основной тариф не найден");
              //return  HttpNotFound();
            }

          

          return tariff.Value;
        }
    }
}