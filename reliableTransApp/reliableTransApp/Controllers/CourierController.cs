﻿using Newtonsoft.Json;
using reliableTransApp.Models;
using reliableTransApp.Models.DataBaseContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace reliableTransApp.Controllers
{
	[Authorize]
    public class CourierController : Controller
    {
        //
        // GET: /Courier/

        public ActionResult Index()
        {
      AccountsManagement accMenag = new AccountsManagement();
      List<UserProfile> users = accMenag.GetUsersInRole("Courier");
      if (users == null) {
        users = new List<UserProfile>();
      }
      return View(users);
        }

        public ActionResult CreateCourier() {
          return View();
        }

    
        //
        // POST: /Town/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void CreateExpressRB(Courier newCourier) {
          if (newCourier != null) {
            new NotImplementedException();
         }

          //Response.Redirect("~/Courier");
        }



        [HttpPost]
        public String GetCouriers(string query) {
          
          

          List<CourierViewModelForDropDown> couriers = null;
          
          if (query != null) {
            CourierModel courierModel = new CourierModel();
            couriers = courierModel.Find(query, 7);
          }

          return JsonConvert.SerializeObject(couriers);
        
        }
    }
}
