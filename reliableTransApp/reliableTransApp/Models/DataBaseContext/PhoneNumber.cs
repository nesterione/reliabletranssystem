﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace reliableTransApp.Models.DataBaseContext {
	/// <summary>
	/// класс: PhoneNumber
	/// 
	/// автор: Нестереня Игорь
	/// дата: 12 сентября 2013 года
	/// 
	/// Класс для хронения номера телефона 
	/// </summary>
	public class PhoneNumber {
		public PhoneNumber()
		{
			Id = Guid.NewGuid();
		}

		[Key]
		public Guid Id { get; set; }

		[Required]
		[StringLength(50)]
		[Display(Name = "Номер телефона")]
		public String Phone { get; set; }

		public Guid ContactId { get; set; }
		public Contact Contact { get; set; }
	}
}
// (с) Copyright 1991-2013 Igor Nesterenya. All Rights Reserved.