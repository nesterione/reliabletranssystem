﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace reliableTransApp.Models.DataBaseContext
{
	public class Cargo
	{
		public Cargo()
		{
			Id = Guid.NewGuid();
		}

		public Guid Id { get; set; }
		public String Name { get; set; }
		public double Lenght { get; set; }
		public double Width { get; set; }
		public double Height { get; set; }
		public double Weight { get; set; }
    public int Count { get; set; }

		public Guid? ExpressRBId { get; set; }
		public virtual ExpressRB ExpressRB { get; set; }
	}
}
