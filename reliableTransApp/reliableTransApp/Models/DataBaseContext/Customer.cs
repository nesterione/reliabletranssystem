﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace reliableTransApp.Models.DataBaseContext
{
	//public class CustomerLERB
	//{
	//public int UPN { get; set; }
	//public String Name { get; set; }
	//public String Ownership { get; set; }
	//Адрес юридический
	//Адрес фактический
	//Банковские реквизиты: найменование банка, БИЛ, р/сч, адрес банка.
	//Номер договора 
	//Срок оплаты счетов
	//Тариф (Общий или индивидуальный)
	//Скидка
	//Контактные телефоны
	//Электронные адреса
	//Факсы
	//}

	public abstract class Customer {
		public Customer()
		{
			Id = Guid.NewGuid();
		}

		[Key]
		public Guid Id { get; set; }

		[Required]
		[StringLength(200)]
		[Display(Name = "Примечание")]
		public String Note { get; set; }

		public Guid ContactId { get; set; }
		[Display(Name = "Контакты")]
		public virtual Contact Contact { get; set; }

		//public Guid ExpressRBId { get; set; }
		//[ForeignKey("ExpressRBId")]
		//public virtual ICollection<ExpressRB> ExpressRBs { get; set; }

		public abstract String GetName();
		//public abstract String GetInfo();
	}

	public class CustomerIRB:Customer
	{
		[Required]
		[Display(Name = "Имя")]
		public String Name { get; set; }

		[Required]
		[Display(Name = "Фамилия")]
		public String Surname { get; set; }

		[Required]
		[Display(Name = "Отчество")]
		public String Patronymic { get; set; }

		//Паспортные данные
		[Display(Name = "Паспортные данные")]
		public Guid PassportDataId { get; set; }
		public PassportData PassportData { get; set; }

		//Адрес 
		[Required]
		[Display(Name = "Адрес")]
		public Guid AddressId { get; set; }
		public virtual Address Address { get; set; }

		public override String GetName()
		{
			return String.Format("{0} {1} {2}", Name, Surname, Patronymic);
		}

		//TODO: Переделать это без html
		public override string ToString()
		{
			String line1 = String.Format("ФИО: {0} {1} {2}", Name, Surname, Patronymic);
			String line2 = String.Format("Регион: {0}", Address.Town.Name);
			String line3 = String.Format("Адрес:{0},{1}",Address.Street, Address.Number);
			
			String line4 = String.Format("Контакты:<br/>");

			foreach (PhoneNumber phoneNumber in Contact.PhoneNumbers)
			{
				line4 += "(тел.)" + phoneNumber.Phone + "<br/>";
			}

			foreach (Email email in Contact.Emailes)
			{
				line4 += "(email)" + email.Address + "<br/>";
			}

			foreach (Fax fax in Contact.Faxes)
			{
				line4 += "(fax)" + fax.Number + "<br/>";
			}

			return String.Format("{0}<br/>{1}<br/>{2}<br/>{3}", line1, line2, line3, line4);
		}
	}

	//public class CustomerLERF
	//{
	//ИНН
	//Наименование
	//Адрес
	//Банковские реквизиты: найменование банка, БИК, р/сч, адрес банка
	//Контактные телефоны
	//Электронные адраса
	//Факсы
	//}

	//public class CustomerIRF
	//{
	//Фамилия 
	//Имя 
	//Отчество

	//Паспортные данные: серия, номер, кем и когда выдан
	//Адрес
	//Телефоны
	//Электронные адрасе
	//}
}