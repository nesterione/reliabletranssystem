﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace reliableTransApp.Models.DataBaseContext
{
	/// <summary>
	/// класс: PassportData
	/// 
	/// автор: Нестереня Игорь
	/// дата: 11 сентября 2013 года
	/// 
	/// Класс для хранения паспортных данных 
	/// </summary>
	public class PassportData
	{
		public PassportData()
		{
			Id = Guid.NewGuid();
		}

		[Key]
		public Guid Id { get; set; }

		[Required]
		[StringLength(5)]
		[Display(Name = "Серия")]
		public String Series { get; set; }

		[Required]
		[Display(Name = "Номер")]
		public int Number { get; set; }

		[Required]
		[Display(Name = "Когда выдан")]
		public DateTime Issued { get; set; }

		[Required]
		[StringLength(200)]
		[Display(Name = "Кем выдан")]
		public String IssuedBy { get; set; }
	}
}
// (с) Copyright 1991-2013 Igor Nesterenya. All Rights Reserved.