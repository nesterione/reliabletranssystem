﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace reliableTransApp.Models.DataBaseContext {
	public class RTransContext : DbContext {
		public RTransContext()
			: base("DefaultConnection")
		{
		}

		public DbSet<Town> Towns { get; set; }
		public DbSet<Courier> Couriers { get; set; }
		public DbSet<PaymentType> PaymentTypes { get; set; }
    public DbSet<Car> Cars { get; set; }
    public DbSet<ExpressRB> ExpressRB { get; set; }
		public DbSet<Cargo> Cargos { get; set; }
		public DbSet<UserProfile> UserProfiles { get; set; }
		public DbSet<PassportData> PassportData { get; set; }
		public DbSet<Address> Addresses { get; set; }
		public DbSet<CustomerIRB> CustomerIRBs { get; set; }
		public DbSet<Fax> Faxes { get; set; }
		public DbSet<Email> Emailes { get; set; }
		public DbSet<PhoneNumber> PhoneNumbers { get; set; }
		public DbSet<Contact> Contact { get; set; }
		public DbSet<Customer> Customer { get; set; }
    public DbSet<Tariff> Tariffs { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<ExpressRB>()
											  .HasRequired(x => x.Sender)
											  .WithMany()
											  .WillCascadeOnDelete(false);

			modelBuilder.Entity<ExpressRB>()
											.HasRequired(x => x.Recipient)
											.WithMany()
											.WillCascadeOnDelete(false);
		}
	}

}