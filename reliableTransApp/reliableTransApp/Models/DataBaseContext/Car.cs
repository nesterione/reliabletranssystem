﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace reliableTransApp.Models.DataBaseContext {
  public class Car {

    public Guid Id { get; set; }

    public Car() {
      Id = Guid.NewGuid();
    }

    /// <summary>
    /// Марка автомобиля
    /// </summary>
    [Display(Name = "Марка автомобиля")]
    public string CarBrand { get; set; }

    /// <summary>
    /// Номер автомобиля
    /// </summary>
    [Display(Name = "Номер автомобиля")]
    public string LicensePlate { get; set; }

    [Display(Name = "Вместимость по объему")]
    public double CubicCapacity { get; set; }

    [Display(Name = "Вместимость по массе")]
    public double WeightCapacity { get; set; }
  }
}