﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace reliableTransApp.Models.DataBaseContext
{
	public class Address
	{
		public Address()
		{
			Id = Guid.NewGuid();
		}

		[Key]
		public Guid Id { get; set; }

		//TODO: Добавить страну, или лучьше добавить к Town, привязку к стране

		//Город
		public Guid TownId { get; set; }
		public virtual Town Town { get; set; }

		[Required]
		[StringLength(200)]
		[Display(Name = "Улица")]
		public String Street { get; set; }

		[Required]
		[StringLength(100)]
		[Display(Name = "Дом")]
		public String Number { get; set; }
	}
}