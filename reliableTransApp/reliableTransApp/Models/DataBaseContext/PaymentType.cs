﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace reliableTransApp.Models.DataBaseContext
{
	public class PaymentType
	{
    public PaymentType() {
      Id = Guid.NewGuid();
    }
		public Guid Id { get; set; }
    public string Name { get; set; }
	}
}
