﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace reliableTransApp.Models.DataBaseContext
{
	public enum Status { 
		[Description("Сохранен")]
		Saved,
		[Description("Заказ принят")]
		Adopted,
		[Description("Передан курьеру")]
		Send,
		[Description("Получен")]
		TakeBack,
		[Description("Доставлен")]
		Delivered,
		[Description("Счет выставлен")]
		Invoice
	};

	public class ExpressRB
	{
		/// <summary>
		/// This class for registration new requests, used form express RB
		/// </summary>
		public ExpressRB()
		{
			Id = Guid.NewGuid();
		}
		
		[Key]
		public Guid Id { get; set; }
    public String BlankNumber { get; set; }
		public DateTime? DateLoading { get; set; }
		public DateTime? DateUnloading { get; set; }
	
		
		public Guid SenderId { get; set; }
		
		[ForeignKey("SenderId")]
		public virtual Customer Sender { get; set; }

		
		public Guid RecipientId {get; set;}
		[ForeignKey("RecipientId")]
		public virtual Customer Recipient { get; set; }

		
		//Payer
// note
		public virtual ICollection<Cargo> Cargos { get; set; }
		public Decimal Price { get; set; }

		public Status Status { get; set; }

    public Guid? CourierOneId { get; set; }
    [ForeignKey("CourierOneId")]
    public virtual Courier CourierOne { get; set; }

    public Guid? CourierTwoId { get; set; }
    [ForeignKey("CourierTwoId")]
    public virtual Courier CourierTwo { get; set; }

	}
}
