﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace reliableTransApp.Models.DataBaseContext
{
	public class Courier
	{
    //TODO: убрать, это излишний ID, не уже не имеет практического значения
		public Courier()
		{
			Id = Guid.NewGuid();
		}

		public Guid Id { get; set; }

    [System.ComponentModel.DataAnnotations.Schema.ForeignKey("UserProfile")]
    public int? UserId { get; set; }
    public virtual UserProfile UserProfile { get; set; }

    /// <summary>
    /// Дата выдачи путевого листа
    /// </summary>
    public DateTime? DateIssueWaybill { get; set; }

    public Guid? PaymentTypeId { get; set; }
		public PaymentType PaymentType { get; set; }

    public Guid? CarId { get; set; }
    public virtual Car Car { get; set; }
  }

  public class CourierViewModelForDropDown {
    public int UserId { get; set; }
    public Guid CourierId { get; set; }
    public String FirstName { get; set; }
    public String SecondName { get; set; }
    public String Patronymic { get; set; }
  }
}
