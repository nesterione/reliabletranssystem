﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace reliableTransApp.Models.DataBaseContext {
  public class Tariff {
    public Tariff()
		{
			Id = Guid.NewGuid();
		}
		public Guid Id { get; set; }
		public String Name { get; set; }
    public double Value { get; set; }
  }
}