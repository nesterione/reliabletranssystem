﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace reliableTransApp.Models.DataBaseContext
{
	public class Contact
	{
		public Contact()
		{
			Id = Guid.NewGuid();
		}

		[Key]
		public Guid Id { get; set; }

		[Display(Name="Номера телефонов")]
		public virtual ICollection<PhoneNumber> PhoneNumbers { get; set; }

		[Display(Name = "Факсы")]
		public virtual ICollection<Fax> Faxes { get; set; }

		[Display(Name = "Электронные адреса")]
		public virtual ICollection<Email> Emailes { get; set; }
	}
}