﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace reliableTransApp.Models.DataBaseContext
{
	public class DBInitializer : DropCreateDatabaseAlways<RTransContext>
	{
		protected override void Seed(RTransContext context)
		{
			context.Towns.Add(new Town { Id = Guid.NewGuid(), Name = "Гомель" });
			context.Towns.Add(new Town { Id = Guid.NewGuid(), Name = "Минск" });
			context.Towns.Add(new Town { Id = Guid.NewGuid(), Name = "Брест" });

			base.Seed(context);
		}
	}
}
