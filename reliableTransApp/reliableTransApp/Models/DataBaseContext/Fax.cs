﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace reliableTransApp.Models.DataBaseContext {
	/// <summary>
	/// класс: Fax
	/// 
	/// автор: Нестереня Игорь
	/// дата: 12 сентября 2013 года
	/// 
	/// Класс для хронения номера факса
	/// </summary>
	public class Fax {
		public Fax()
		{
			Id = Guid.NewGuid();
		}

		[Key]
		public Guid Id { get; set; }

		[Required]
		[StringLength(50)]
		[Display(Name = "Номер факса")]
		public String Number { get; set; }

		public Guid ContactId { get; set; }
		public Contact Contact { get; set; }
	}
}
// (с) Copyright 1991-2013 Igor Nesterenya. All Rights Reserved.