﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace reliableTransApp.Models.DataBaseContext
{
	public class Town
	{
		public Town()
		{
			Id = Guid.NewGuid();
		}

		[Key]
		public Guid Id { get; set; }

		[Required]
		[StringLength(100)]
		[Display(Name = "Название города")]
		public string Name { get; set; }

		//public virtual Country Country { get; set; }
	}
}
