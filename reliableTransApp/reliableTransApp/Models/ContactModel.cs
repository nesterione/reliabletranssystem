﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using reliableTransApp.Models.DataBaseContext;

namespace reliableTransApp.Models {
	/// <summary>
	/// класс: ContactModel
	/// 
	/// автор: Нестереня Игорь
	/// дата: 15 сентября 2013 года
	/// 
	/// Класс для предоставления доступа к таблице Town
	/// </summary>
	public class ContactModel {

		private RTransContext db = new RTransContext();

		//public List<Town> GetAll()
		//{
		//	return db.Towns.ToList();
		//}

		//public Town Get(Guid id)
		//{
		//	return db.Towns.Find(id);
		//}

		//public void Delete(Guid id)
		//{
		//	Town town = db.Towns.Find(id);
		//	db.Towns.Remove(town);
		//	db.SaveChanges();
		//}

		//public void Update(Town newTown)
		//{
		//	db.Entry(newTown).State = EntityState.Modified;
		//	db.SaveChanges();
		//}

		public Guid Create(Contact newContact)
		{
			//Создание контакта
			db.Contact.Add(newContact);

			Guid IdContact = newContact.Id;

      
        //Добавление номеров телефонов
      if (newContact.PhoneNumbers != null) {
        foreach (DataBaseContext.PhoneNumber newPhoneNumber in newContact.PhoneNumbers) {
          if (newPhoneNumber != null) {
            newPhoneNumber.ContactId = IdContact;
            db.PhoneNumbers.Add(newPhoneNumber);
          }
        }
      }
			//Добавление электронных адресов
      if (newContact.Emailes != null) {
        foreach (DataBaseContext.Email newEmail in newContact.Emailes) {
          if (newEmail != null) {
            newEmail.ContactId = IdContact;
            db.Emailes.Add(newEmail);
          }
        }
      }

			//Добавление номеров факсов
      if (newContact.Faxes != null) {
        foreach (DataBaseContext.Fax newFax in newContact.Faxes) {
          if (newFax != null) {
            newFax.ContactId = IdContact;
            db.Faxes.Add(newFax);
          }
        }
      }

			db.SaveChanges();

			return newContact.Id;
		}


	}
}
// (с) Copyright 1991-2013 Igor Nesterenya. All Rights Reserved.