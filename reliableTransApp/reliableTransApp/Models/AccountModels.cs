﻿using reliableTransApp.Models.DataBaseContext;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace reliableTransApp.Models {
  [Table("UserProfile")]
  public class UserProfile {
    [Key]
    [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
    public int UserId { get; set; }
    public string UserName { get; set; }


    public string Name { get; set; }
    public string Surname { get; set; }
    public string Patronymic { get; set; }

    [Display(Name = "Адрес")]
    public String Address { get; set; }

    //Паспортные данные
    [Display(Name = "Паспортные данные")]
    public Guid? PassportDataId { get; set; }
    public virtual PassportData PassportData{ get; set; }

    public Guid? ContactId { get; set; }
    [Display(Name = "Контакты")]
    public virtual Contact Contact { get; set; }
  }

  public class RegisterExternalLoginModel {
    [Required]
    [Display(Name = "Имя пользователя")]
    public string UserName { get; set; }

    public string ExternalLoginData { get; set; }
  }

  public class LocalPasswordModel {
    [Required]
    [DataType(DataType.Password)]
    [Display(Name = "Текущий пароль")]
    public string OldPassword { get; set; }

    [Required]
    [StringLength(100, ErrorMessage = "Значение \"{0}\" должно содержать не менее {2} символов.", MinimumLength = 6)]
    [DataType(DataType.Password)]
    [Display(Name = "Новый пароль")]
    public string NewPassword { get; set; }

    [DataType(DataType.Password)]
    [Display(Name = "Подтверждение пароля")]
    [Compare("NewPassword", ErrorMessage = "Новый пароль и его подтверждение не совпадают.")]
    public string ConfirmPassword { get; set; }
  }

  public class LoginModel {
    [Required]
    [Display(Name = "Имя пользователя")]
    public string UserName { get; set; }

    [Required]
    [DataType(DataType.Password)]
    [Display(Name = "Пароль")]
    public string Password { get; set; }

    [Display(Name = "Запомнить меня")]
    public bool RememberMe { get; set; }
  }

  public class RegisterModel {
    [Required]
    [Display(Name = "Имя пользователя")]
    public string UserName { get; set; }

    [Required]
    [StringLength(100, ErrorMessage = "Значение \"{0}\" должно содержать не менее {2} символов.", MinimumLength = 6)]
    [DataType(DataType.Password)]
    [Display(Name = "Пароль")]
    public string Password { get; set; }

    [DataType(DataType.Password)]
    [Display(Name = "Подтверждение пароля")]
    [Compare("Password", ErrorMessage = "Пароль и его подтверждение не совпадают.")]
    public string ConfirmPassword { get; set; }

    [Display(Name = "Настоящее имя пользователя")]
    public string Name { get; set; }

    [Display(Name = "Фамилия  пользователя")]
    public string Surname { get; set; }

    [Display(Name = "Отчество пользователя")]
    public string Patronymic { get; set; }

    //Паспортные данные
    public Guid? PassportDataId { get; set; }
    [Display(Name="Паспортные данные")]
    public PassportData PassportData { get; set; }

    [Display(Name = "Адрес")]
    public String Address { get; set; }

    [Display(Name = "Номера телефонов!")]
    public List<PhoneNumber> PhoneNumbers { get; set; }

    [Display(Name = "Факсы")]
    public List<Fax> Faxes { get; set; }

    [Display(Name = "Электронные адреса")]
    public List<Email> Emailes { get; set; }

    [Display(Name="Данные курьера")]
    public Courier Courier { get; set; }
    
    [Display(Name = "Контактные данные")]
    public Guid? ContactId { get; set; }
  }

  public class ExternalLogin {
    public string Provider { get; set; }
    public string ProviderDisplayName { get; set; }
    public string ProviderUserId { get; set; }
  }

  public class AccountsManagement {

    RTransContext db = new RTransContext();

    public List<UserProfile> GetUsersInRole(String role) {
      var usernames = Roles.GetUsersInRole(role);

      var users = db.UserProfiles
       .Where(x => usernames.Contains(x.UserName)).ToList();

      return users;
    }

    public UserProfile GetUser(String userName) {
      return db.UserProfiles.SingleOrDefault(u => u.UserName == userName);
    }
  }

}