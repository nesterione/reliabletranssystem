﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using reliableTransApp.Models.DataBaseContext;

namespace reliableTransApp.Models {
	public class CustomerModel {

		RTransContext db = new RTransContext();

		public List<DataBaseContext.Customer> Find(String query, int maxRows)
		{
			IEnumerable<Customer> customers = db.CustomerIRBs.Where(
				customer => customer.Name.Contains(query)||
				customer.Surname.Contains(query)||
				customer.Patronymic.Contains(query)).Take(maxRows);
			
			return customers.ToList();
		}

		public List<DataBaseContext.Customer> GetAll()
		{
			return db.Customer.ToList();
		}
	}
}