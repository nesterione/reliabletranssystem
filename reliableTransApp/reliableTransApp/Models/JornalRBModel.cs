﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using reliableTransApp.Models.DataBaseContext;

namespace reliableTransApp.Models {
  /// <summary>
  /// класс: JornalRBModel
  /// 
  /// автор: Нестереня Игорь
  /// дата: 08 сентября 2013 года
  /// 
  /// Класс для управления заказами, создания новых, редактирования и удаления
  public class JornalRBModel {
    private RTransContext db = new RTransContext();
    private const int DAYS_ACTUAL = 3;

    public List<ExpressRB> GetAll() {
      return db.ExpressRB.ToList();
    }

    /// <summary>
    /// Возвращает актуальные записи отсортированные по дате разгрузки
    /// 
    /// Актуальными записями считаются DAYS_ACTUAL для до сегодняшнего для
    /// и DAYS_ACTUAL для после сегодняшнего дня
    /// </summary>
    /// <returns></returns>
    public List<ExpressRB> GetActual() {
      DateTime dateStart = DateTime.UtcNow.Subtract(TimeSpan.FromDays(DAYS_ACTUAL));
      DateTime dateEnd = DateTime.UtcNow.Add(TimeSpan.FromDays(DAYS_ACTUAL));

      return db.ExpressRB.Where(
        express =>
        express.DateUnloading >= dateStart &&
        express.DateUnloading <= dateEnd
        ).ToList();
    }

    public void Create(ExpressRB newExpressRB) {

      if (newExpressRB.Status == Status.Delivered || newExpressRB.Status == Status.Invoice) {
        bool isCorrect = (newExpressRB.BlankNumber == null) || (newExpressRB.BlankNumber.Trim().Length < 1);
        if (isCorrect) {
          throw new Exception("Нельзя установить статусы \"Счет выставлени\" и \"Груз доставлен\" пока не будет заполнен номер адресного бланка");
        }
      }

      db.ExpressRB.Add(newExpressRB);

      // Изменения должны быть отправлены в базу перед тем как 
      // будут добавляться грузы.
      db.SaveChanges();
    }

    public ExpressRB Get(Guid id) {
      return db.ExpressRB.Find(id);
    }

    public void Update(ExpressRB updExpressRB) {
      var original = db.ExpressRB.Find(updExpressRB.Id);

      if (original != null) {
        db.Entry(original).CurrentValues.SetValues(updExpressRB);
        db.SaveChanges();
      }
    }
  }
}
// (с) Copyright 1991-2013 Igor Nesterenya. All Rights Reserved.