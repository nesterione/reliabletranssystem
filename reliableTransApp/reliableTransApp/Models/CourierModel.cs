﻿using reliableTransApp.Models.DataBaseContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace reliableTransApp.Models {
  public class CourierModel {

    RTransContext db = new RTransContext();

    public void Create(Courier newCourier) {
      db.Couriers.Add(newCourier);
      db.SaveChanges();
    }
    public Courier Get(Guid Id) { throw new NotImplementedException(); }
    public List<Courier> GetAll() { throw new NotImplementedException(); }

    /// <summary>
    /// Для подтверждения удаления вызвать SaveChanges()
    /// </summary>
    /// <param name="userId"></param>
    public void DeleteByUserId(int userId) {

      var couriers = db.Couriers.Where(w => w.UserId == userId).ToList();
      if (couriers != null) {
        CarModel carModel = new CarModel();

        foreach (var courier in couriers) {
          Guid? couId = courier.CarId;

          db.Couriers.Remove(courier);
          db.SaveChanges();

          if (couId != null) {
            carModel.Delete((Guid)couId);
          }
        }
      }
    }
    //TODO: сделать связь как нибудь более удобно
    public Guid? GetIdByUserId(int userId) {
      var d = db.Couriers.Where(w => w.UserId == userId).FirstOrDefault();
      if (d != null) {
        return d.Id;
      } else
        return null;
    }

    public void Update() { throw new NotImplementedException(); }

    public List<CourierViewModelForDropDown> Find(String query, int maxRows) {
      const String COURIER_ROLE = "Courier";

      // TODO: в случае нехватки скорости кешировать значения пользователей
      var usernames = Roles.GetUsersInRole(COURIER_ROLE);

      var users = db.UserProfiles.Where(x => (usernames.Contains(x.UserName)) && (x.Name.Contains(query) ||
                                                                                x.Surname.Contains(query) ||
                                                                                x.Patronymic.Contains(query))).Take(maxRows)
                                                                                .Select(ff =>
                                                                                  new CourierViewModelForDropDown {
                                                                                    UserId = ff.UserId,
                                                                                    FirstName = ff.Name,
                                                                                    SecondName = ff.Surname,
                                                                                    Patronymic = ff.Patronymic
                                                                                  }).ToList();
      
      //Нужно выбрать id курьеров из таблицы курьеров
      List<CourierViewModelForDropDown> retList = new List<CourierViewModelForDropDown>();
      CourierModel courModel = new CourierModel();
      for (int i = 0; i < users.Count; i++) {
        Guid? userID = courModel.GetIdByUserId(users[i].UserId);
        if (userID != null) { users[i].CourierId = (Guid)userID; retList.Add(users[i]); } 
      }

      

      return retList;
    }

    //public void SaveChanges() {
    //  db.SaveChanges();
    //}
  }
}