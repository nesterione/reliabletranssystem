﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using reliableTransApp.Models.DataBaseContext;

namespace reliableTransApp.Models {
  public class CarModel {
    RTransContext db = new RTransContext();

    public void Delete(Guid Id) {
      Car car = db.Cars.Where(x => x.Id == Id).FirstOrDefault();
      if (car != null) {
        db.Cars.Remove(car);
        db.SaveChanges();
      }
    }

    public void Delete(Car car) {
      if(car!= null) {
        db.Cars.Remove(car);
        db.SaveChanges();
      }
    }
    /*
    public void SaveChanges() {
      db.SaveChanges();
    }*/
  }
}