﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace reliableTransApp.Models {
	public class CustomerIRBModel {
		DataBaseContext.RTransContext db = new DataBaseContext.RTransContext();

		public void Create(DataBaseContext.CustomerIRB newCustomerIRB)
		{
			if (newCustomerIRB != null && newCustomerIRB.Id != null)
			{
				db.CustomerIRBs.Add(newCustomerIRB);
				db.SaveChanges();
			}
			else
			{
				throw new Exception("Некорректный клиент");
			}
		}

		public void Delete(Guid Id)
		{
			db.Customer.Remove(db.Customer.Where(customer => customer.Id == Id).First());
			db.SaveChanges();
		}
	}
}