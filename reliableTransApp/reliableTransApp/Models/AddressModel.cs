﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace reliableTransApp.Models {
	public class AddressModel {

		DataBaseContext.RTransContext db = new DataBaseContext.RTransContext();

		public void Create(DataBaseContext.Address address)
		{
			if (address.Id != null)
			{
				db.Addresses.Add(address);
				db.SaveChanges();
			}
			else
			{
				throw new Exception("Не указан Id");
			}
		}
	}
}