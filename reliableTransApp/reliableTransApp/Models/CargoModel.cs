﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using reliableTransApp.Models.DataBaseContext;
using System.Data;

namespace reliableTransApp.Models
{
	/// <summary>
	/// класс: CargoModel
	/// 
	/// автор: Нестереня Игорь
	/// дата: 08 сентября 2013 года
	/// 
	/// Класс для предоставления доступа к таблице грузов
	/// </summary>
	public class CargoModel
	{
		private RTransContext db = new RTransContext();

		/*
		public List<Cargo> GetAll()
		{
			return db.Towns.ToList();
		}*/
		/*
		public Cargo Get(Guid id)
		{
			return db.Cargos.Find(id);
		}

		public void Delete(Guid id)
		{
			Cargo cargo = db.Cargos.Find(id);
			db.Cargos.Remove(cargo);
			db.SaveChanges();
		}

		public void Update(Cargo newCargo)
		{
			db.Entry(newCargo).State = EntityState.Modified;
			db.SaveChanges();
		}*/

		public void Create(Cargo newCargo)
		{
			newCargo.Id = Guid.NewGuid();
			db.Cargos.Add(newCargo);
			db.SaveChanges();
		}
	}
}
// (с) Copyright 1991-2013 Igor Nesterenya. All Rights Reserved.