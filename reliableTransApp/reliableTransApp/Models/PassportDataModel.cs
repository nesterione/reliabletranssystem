﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using reliableTransApp.Models.DataBaseContext;

namespace reliableTransApp.Models {
	public class PassportDataModel {

		private RTransContext db = new RTransContext();

		public void Create(PassportData newPassport)
		{
			if (newPassport.Id != default(Guid))
			{
				db.PassportData.Add(newPassport);
				db.SaveChanges();
			}
		}
	}
}