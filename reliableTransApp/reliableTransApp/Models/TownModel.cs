﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using reliableTransApp.Models.DataBaseContext;
using System.Data;
using System.Collections.Generic;

namespace reliableTransApp.Models {
	/// <summary>
	/// класс: TownModel
	/// 
	/// автор: Нестереня Игорь
	/// дата: 07 сентября 2013 года
	/// 
	/// Класс для предоставления доступа к таблице Town
	/// </summary>
	public class TownModel {
		private RTransContext db = new RTransContext();

		public List<Town> GetAll()
		{
			return db.Towns.ToList();
		}

		public List<Town> GetFind(String query, int maxCnt)
		{
			IEnumerable<Town> towns = db.Towns.Where(town => town.Name.Contains(query)).Take(maxCnt);
			return towns.ToList();
		}

		public Town Get(Guid id)
		{
			return db.Towns.Find(id);
		}

		public void Delete(Guid id)
		{
			Town town = db.Towns.Find(id);
			db.Towns.Remove(town);
			db.SaveChanges();
		}

		public void Update(Town newTown)
		{
			db.Entry(newTown).State = EntityState.Modified;
			db.SaveChanges();
		}

		public void Create(Town newTown)
		{
			newTown.Id = Guid.NewGuid();
			db.Towns.Add(newTown);
			db.SaveChanges();
		}
	}
}
// (с) Copyright 1991-2013 Igor Nesterenya. All Rights Reserved.