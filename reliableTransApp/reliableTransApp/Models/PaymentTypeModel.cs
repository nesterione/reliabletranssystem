﻿using reliableTransApp.Models.DataBaseContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace reliableTransApp.Models {
  public class PaymentTypeModel {
    RTransContext db = new RTransContext();

    public List<PaymentType> GetAll() {
      return db.PaymentTypes.ToList();
    }
  }
}