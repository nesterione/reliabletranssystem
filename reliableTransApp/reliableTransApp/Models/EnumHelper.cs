﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace reliableTransApp.Models {
	public static class EnumHelper {

		/// <summary>
		/// Возращает значение атрибуда [Description] для поля перечисления
		/// Или его навзвание в ином случае
		/// </summary>
		/// <param name="enumVal"></param>
		/// <returns></returns>
		public static String getDescriptionAttr(Enum enumVal)
		{
			var filds = enumVal.GetType().GetField(enumVal.ToString());
			DescriptionAttribute[] att = (DescriptionAttribute[])filds.GetCustomAttributes(typeof(DescriptionAttribute), false);
			if (att.Length == 0)
			{
				return enumVal.ToString();
			}
			else
			{
				return att[0].Description;
			}
		}
	}
}