﻿/// <reference path="../reliableTrans.js" />

$(function () {
	main();
});

function main() {
	setRusianCalender();
	//Добавление функции для обработки добавления нового клиента
	function create_customer_click(e) {
		e.preventDefault();
		var newCustomer = formCustomerIRB('data-customer-irb')

		addCustomerIRB(newCustomer, function (msg) {

			var obj = $.parseJSON(fixJSON(msg));

			var tr = document.createElement('tr');
			tr.id = obj.Id;

			var td1 = document.createElement('td');

			var linkDetails = createControlLink('javascript:alert("Не реализованно");', 'Подробно', 'glyphicon-eye-open');
			var linkEdit = createControlLink('javascript:alert("Не реализованно");', 'Редактировать', 'glyphicon-pencil');
			var linkDelete = createControlLink('javascript:delCustomer("' + obj.Id + '");', 'Удалить', 'glyphicon-remove');
			td1.appendChild(linkDetails);
			td1.appendChild(linkEdit);
			td1.appendChild(linkDelete);

			var td2 = document.createElement('td');
			td2.innerText = obj.Name;
			tr.appendChild(td1);
			tr.appendChild(td2);

			$(tr).prependTo('#customersTable');
		});
		$('#data-customer-irb .customer-form').modal('hide');
	};

	configureCustomerForm(create_customer_click, 'data-customer-irb');
}

function createControlLink(linkHref,linkTitle, imageClass)
{
	var link = document.createElement('a');
	link.href = linkHref;
	link.title = linkTitle;

	var span = document.createElement('span');
	span.classList.add('glyphicon');
	span.classList.add(imageClass);

	link.appendChild(span);

	return link;
}

function delCustomer(customerId) {
	if (confirm("Подтвердите удаление")) {
		deleteCustomer(customerId, function (msg) { if (msg = "true") { $('#' + customerId).remove(); }});
	}

	return false;
}