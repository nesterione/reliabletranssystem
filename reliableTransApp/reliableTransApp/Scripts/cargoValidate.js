﻿$(document).ready(function () {
	//TODO: Убрать глобальную переменную
	var m_cntCargo = 0;
	function formInputCargo(inValue, inProperty) {

		var rez = "<td> <input  name='Cargoes[" + m_cntCargo +
								 "]." + inProperty +
								 "' type='hidden' value='" + inValue +
								 "'/>" + inValue +
								 "</td>";
		return rez.toString();
	}

	var name = $("#name"),
			count = $("#count"),
			lenght = $("#lenght"),
			width = $("#width"),
			height = $("#height"),
			weight = $("#weight"),
			allFields = $([]).add(name).add(count).add(lenght).add(width).add(height).add(weight),
		tips = $(".validateTips");

	$("#cargo_form").validate({

		submitHandler: function (form) {

			$("#CargoesTable tbody").append("<tr>" +
				formInputCargo(name.val(), "Name") +
				formInputCargo(count.val(), "Count") +
				formInputCargo(lenght.val(), "Lenght") +
				formInputCargo(width.val(), "Width") +
				formInputCargo(height.val(), "Height") +
				formInputCargo(weight.val(), "Weight") +
			"</tr>");

			m_cntCargo++;

			allFields.val("");
			$('#createCargo').modal('hide');
		},

		rules: {

			cargo_name: {
				required: true,
				minlength: 2,
				maxlength: 16,
			},
			cargo_count: {
				required: true,
				number: true,
				maxlength: 16,
			},
			cargo_lenght: {
				required: true,
				number: true,
				maxlength: 16,
			},
			cargo_width: {
				required: true,
				number: true,
				maxlength: 16,
			},
			cargo_height: {
				required: true,
				number: true,
				maxlength: 16,
			},
			cargo_weight: {
				required: true,
				number: true,
				maxlength: 16,
			},
		},

		messages: {
			cargo_name: {
				required: "Это поле обязательно для заполнения",
				minlength: "Название минимум 2 символа",
				maxlength: "Максимальное число символо - 16",
			},
			cargo_count: {
				required: "Это поле обязательно для заполнения",
				number: "Должно быть число",
				maxlength: "Максимальное число символо - 10",
			},
			cargo_lenght: {
				required: "Это поле обязательно для заполнения",
				number: "Должно быть число",
				maxlength: "Максимальное число символо - 10",
			},
			cargo_width: {
				required: "Это поле обязательно для заполнения",
				number: "Должно быть число",
				maxlength: "Максимальное число символо - 10",
			},
			cargo_height: {
				required: "Это поле обязательно для заполнения",
				number: "Должно быть число",
				maxlength: "Максимальное число символо - 10",
			},
			cargo_weight: {
				required: "Это поле обязательно для заполнения",
				number: "Должно быть число",
				maxlength: "Максимальное число символо - 10",
			},



		}

	});


}); //end of ready