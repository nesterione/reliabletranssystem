﻿/// <reference path="../reliableTrans.js" />
/// <reference path="../jquery-1.9.1.js" />
/// <reference path="../jquery-ui-1.10.3.js" 
/// <reference path="../jquery.json-2.4.min.js" />
/// <reference path="../bootstrap.js" />

/**
 * Скрипты для обработки формы Create, для формы ЭкспрессРБ
 *
 * @author Igor Nesterenya
 * @version 0.01
 */
$(function () {

	//Вызов функции инициализации
	main();

	function showCourier(query, id_dropdown, id_for_courier_id, id_for_view) {
		var host = '//' + window.location.host + '/Courier/GetCouriers';
		var obj = new Object();
		obj.query = query

		var json = $.toJSON(obj);

		var request = $.ajax({
			type: 'POST',
			url: host,
			data: json,
			contentType: 'application/json; charset=utf-8'
		});

		request.done(function (msg) {

			var id_result = "#" + id_dropdown + " li";
			$(id_result).remove();
			
			var rezultMsg = $.parseJSON(fixJSON(msg));
			

			$.each(rezultMsg, function () {
				
				var thisCustomer = this;

				var li_node = document.createElement('li');
				li_node.setAttribute('role','presentation');

				var a_node = document.createElement('a');
				
				a_node.setAttribute('role','menuitem');
				a_node.href="#";
				a_node.innerText = thisCustomer.FirstName + " " + thisCustomer.SecondName + " " + thisCustomer.Patronymic;
				a_node.courier = thisCustomer;
				
				
				a_node.onclick = function() {
					$("#" + id_for_courier_id).val(thisCustomer.CourierId);
					$("#" + id_for_view).text(thisCustomer.FirstName + " " + thisCustomer.SecondName + " " + thisCustomer.Patronymic);

				}

				li_node.appendChild(a_node);

				$('#'+id_dropdown).append($(li_node));
				
			});
		});

		request.fail(function (jqXHR, textStatus) {
			alert("Request failed: " + textStatus);
		});

		return false;
	}

	function showCustomers(customerQuery) {

		var host = '//' + window.location.host + '/Customer/GetCustomers';

		var obj = new Object();
		obj.query = customerQuery

		var json = $.toJSON(obj);

		var request = $.ajax({
			type: 'POST',
			url: host,
			data: json,
			contentType: 'application/json; charset=utf-8'
		});

		request.done(function (msg) {
			$('#dropDown_sender li').remove();
			////TODO: Переписать это бред
			var obj = $.parseJSON(fixJSON(msg));
			////	
			var Number = 0;

			$.each(obj, function () {

				var className = "customer_" + Number;
				Number++;
				debugger;

				var thisCustomer = this;
				$('<li role="presentation id=' + className + '"><a role="menuitem" id="' + className + 'link' + '"  tabindex="-1"   href="#">' + this.Name + '</a> <input id="' + className + 'name' + '" type="hidden" value="' + this.Name + '"> <input id="' + className + 'id' + '" type="hidden" value="' + this.Id + '"></li>').appendTo('#dropDown_sender');

				$('#' + className + 'link').click(function () {
					$('#SenderContext').html(thisCustomer.Description);
					$('#SenderId').val(thisCustomer.Id);
					$('#Sender').val("");
				});
			});

		});

		request.fail(function (jqXHR, textStatus) {
			alert("Request failed: " + textStatus);
		});

		return false;
	}

	function showCustomersR(customerQuery) {

		var host = '//' + window.location.host + '/Customer/GetCustomers';
		var obj = new Object();
		obj.query = customerQuery

		var json = $.toJSON(obj);

		var request = $.ajax({
			type: 'POST',
			url: host,
			data: json,
			contentType: 'application/json; charset=utf-8'
		});

		request.done(function (msg) {

			$('#dropDown_recipient li').remove();
			////TODO: Переписать это бред
			var obj = $.parseJSON(fixJSON(msg));
			////	
			var Number = 0;

			$.each(obj, function () {

				var className = "customerR_" + Number;
				Number++;

				var thisCustomer = this;
				$('<li role="presentation id=' + className + '"><a role="menuitem" id="' + className + 'link' + '"  tabindex="-1"   href="#">' + this.Name + '</a> <input id="' + className + 'name' + '" type="hidden" value="' + this.Name + '"> <input id="' + className + 'id' + '" type="hidden" value="' + this.Id + '"></li>').appendTo('#dropDown_recipient');

				$('#' + className + 'link').click(function () {
					$('#RecipientContext').html(thisCustomer.Description);
					$('#RecipientId').val(thisCustomer.Id);
					$('#Recipient').val("");
				});
			});

		});

		request.fail(function (jqXHR, textStatus) {
			alert("Request failed: " + textStatus);
		});

		return false;
	}

	function main() {
		setRusianCalender();

		
		
		$('.option11').click(function () {
			$('#option1').button('reset');
			$('#option2').button('reset');
			$('#option2').button('complete');
			$('option1.nav-tabs').button()

		});

		configureCustomerForm(add_customer_irb_click, 'data-customer-irb');

		$('#Sender').keyup(function () { showCustomers($('#Sender').val()); });
		$('#Recipient').keyup(function () { showCustomersR($('#Recipient').val()); });

		$('#courier-one').keyup(function () { showCourier($('#courier-one').val(), "dropDown_courier-one",  "courier-one-id", "courier-one-context"); });
		$('#courier-two').keyup(function () { showCourier($('#courier-two').val(), "dropDown_courier-two", "courier-two-id", "courier-two-context"); });
	}
	
});