﻿/// <reference path="jquery-1.9.1.min.js" />
/// <reference path="jquery.json-2.4.min.js" />

/**
 * Здесь содержатся скрибты для обработки 
 * основных действий Добавление контактной информации и др.
 *
 * @author Igor Nesterenya
 * @version 0.01
 */

/**
* Устанавливает настройки для календаря jQuery, для отображения данных на русском
*/
function setRusianCalender() {
	$.datepicker.regional['ru'] = {
		closeText: 'Закрыть',
		prevText: '&#x3c;Пред',
		nextText: 'След&#x3e;',
		currentText: 'Сегодня',
		monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
		'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
		monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
		'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
		dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
		dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
		dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
		weekHeader: 'Нед',
		dateFormat: 'dd.mm.yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	};

	$.datepicker.setDefaults($.datepicker.regional['ru']);

	$(".datepicker").datepicker(
		{

			changeMonth: true,
			changeYear: true,
		});

	return false;
}

/*
* Формирует объект содержащий контактные данные из блоков
* находящегося внутри блока с переданным в параметре id
*
* @param {string} ownerId id внешнего блока.
* @return {Contact} Новый объект Contact.
*/
function formContactData(ownerId) {
	var selectorId = '#' + ownerId + ' ';

	var newContact = new Object();

	var phoneArray = [];
	$(selectorId + '.contact-phones :text').each(function () {
		if ($(this).val() != "") {
			var obPhone = new Object();
			obPhone.Phone = $(this).val();
			phoneArray.push(obPhone);
		};
	});

	var emailsArray = [];
	$(selectorId + '.contact-emails :text').each(function () {
		if ($(this).val() != "") {
			var obEmaile = new Object();
			obEmaile.Address = $(this).val();
			emailsArray.push(obEmaile);
		};
	});

	var faxesArray = [];
	$(selectorId + '.contact-faxes :text').each(function () {
		if ($(this).val() != "") {
			var obFax = new Object();
			obFax.Number = $(this).val();
			faxesArray.push(obFax);
		};
	});

	newContact.PhoneNumbers = phoneArray;
	newContact.Emailes = emailsArray;
	newContact.Faxes = faxesArray;

	return newContact;
}

/*
* Формирует объект содержащий паспортные данные
* для блока с данными оходящимися внутри блока
* и переданным в параметре id
*
* @param {string} ownerId Внешний id.
* @return {PassportData} Новый объект PassportData.
*/
function formPassportData(ownerId) {
	var selectorId = '#' + ownerId + ' ';

	var newPassData = new Object();
	newPassData.Series = $(selectorId + '.passport-series').val();
	newPassData.Number = $(selectorId + '.passport-number').val();
	newPassData.Issued = $(selectorId + '.passport-date-issues').val();
	newPassData.IssuedBy = $(selectorId + '.passport-issues-by').val();

	return newPassData;
}

/*
* Формирует объект содержащий адресс из частичного представления
* находящегося внутри блока с переданным в параметре id
*
* @param {string} ownerId id внешнего блока.
* @return {Address} Новый объект Address.
*/
function formAddress(ownerId) {
	var selectorId = '#' + ownerId + ' ';

	var newAddress = new Object();
	newAddress.TownId = $(selectorId + '.address-town-id').val();
	newAddress.Street = $(selectorId + '.address-street').val();
	newAddress.Number = $(selectorId + '.address-house').val();

	return newAddress;
}

/*
* Формирует объект содержащий данные о клиенте
* физического лица Республики Беларусь
*
* @return {CustomerIRB} Новый объект CustomerIRB.
*/
function formCustomerIRB(ownerId) {
	var selectorId = '#' + ownerId + ' ';

	var newCustomer = new Object();
	newCustomer.Name = $(selectorId + '.customer-name').val();
	newCustomer.Surname = $(selectorId + '.customer-surname').val();
	newCustomer.Patronymic = $(selectorId + '.customer-patronymic').val();
	newCustomer.Note = $(selectorId + '.customer-note').val();
	
	newCustomer.PassportData = formPassportData(ownerId);
	newCustomer.Contact = formContactData(ownerId);
	newCustomer.Address = formAddress(ownerId);

	return newCustomer;
}

function addCustomerIRB(newCustomer, fnDone) {
	var json = $.toJSON(newCustomer);
	var host = window.location.host;
	var request = $.ajax({
		type: 'POST',
		url: '//' + host + '/Customer/AddNewCustomerIRB',
		data: json,
		contentType: 'application/json; charset=utf-8'
	});

	request.done(fnDone);

	request.fail(function (jqXHR, textStatus) {
		alert("Request failed: " + textStatus);
	});
	return false;
}

// @param {function} query функция выполняемая при нажатии на кнопку добавления клиента.
// @param {string} ownerId Id внешнего блока.
function configureCustomerForm(addCustomerClick, ownerId) {
	var selectorId = '#' + ownerId + ' ';

	//Добавление функции для обработки добавления нового клиента
	$(selectorId + '.customer-add').click(addCustomerClick);

	$(selectorId + '.create-town').click(function () {
		$(selectorId + '.town-form').modal('show');
	}
	);

	//Конфигурация добавления контактных данных

	configureAutoaddedContactFilds(ownerId);

	//Конфигурация добавления городов из формы добавления клиентов
	$(selectorId + '.town-add').click(function () {
		createTown($(selectorId + '.town-name').val(), function (rezult) {

			var obj = $.parseJSON(fixJSON(rezult));
			$(selectorId + '.address-town-name').text(obj.Name);
			$(selectorId + '.address-town-id').val(obj.Id);
			$(selectorId + '.town-form').modal('hide');
			$(selectorId + '.customer-form').modal('show');
		});
	});

	//При закрытии формы добавления города паказать обратно форму редактирования клиента
	$(selectorId + '.town-form').on('hidden.bs.modal', function () {
		$(selectorId + '.customer-form').modal('show');
	});


	//Конфигурация подсказки городов из базы данных
	$(selectorId + '.town-chooser').keyup(function () {
		var query = $(selectorId + '.town-chooser').val();
		showTowns(query, ownerId);
	});
}

function configureAutoaddedContactFilds(ownerId) {

	//Конфигурация добавления контактных данных
	createAutoAddedRow('creater-phone', "contact-phones", ownerId);
	createAutoAddedRow('creater-email', "contact-emails", ownerId);
	createAutoAddedRow('creater-fax', "contact-faxes", ownerId);

}

function add_customer_irb_click(e) {
	e.preventDefault();
	var newCustomer = formCustomerIRB('data-customer-irb')
	addCustomerIRB(newCustomer);
	$('#data-customer-irb .customer-form').modal('hide');
}

function deleteCustomer(customerID, done) {

	var obj = new Object();
	obj.customerID = customerID;

	var json = $.toJSON(obj);
	var host = window.location.host;
	var request = $.ajax({
		type: 'POST',
		url: '//' + host + '/Customer/Delete',
		data: json,
		contentType: 'application/json; charset=utf-8'
	});

	request.done(done);

	request.fail(function (jqXHR, textStatus) {
		alert("Request failed: " + textStatus);
	});

	return false;
}

/*
* Отображает города из базы данных подходящих по запросу
* для блока с данными оходящимися внутри блока
* и переданным в параметре id
*
* @param {string} query Запрос.
* @param {string} ownerId Внешний id.
* @return {PassportData} Новый объект PassportData.
*/
function showTowns(nameTown, ownerId) {
	var selectorId = '#' + ownerId + ' ';

	var obj = new Object();
	obj.query = nameTown;
	var json = $.toJSON(obj);

	var request = $.ajax({
		type: 'POST',
		url: '//' + window.location.host + '/Town/GetTowns',
		data: json,
		contentType: 'application/json; charset=utf-8'
	});

	request.done(function (msg) {

		var obj = $.parseJSON(fixJSON(msg));
		var Number = 0;

		$(selectorId + '.dropdown-town li').remove();
		$.each(obj, function () {
			var current_town = this;

			var new_li = document.createElement('li');
			new_li.setAttribute('role', 'presentation');

			var in_div = document.createElement('a');
			in_div.innerText = current_town.Name;
			in_div.href = '#';
			in_div.onclick = function () {
				$(selectorId + ".address-town-name").text(current_town.Name);
				$(selectorId + ".address-town-id").val(current_town.Id);
				$(selectorId + '.town-chooser').val("");
			}

			new_li.appendChild(in_div);
			$(new_li).appendTo(selectorId + '.dropdown-town');
		});
	});

	request.fail(function (jqXHR, textStatus) {
		alert("Request failed: " + textStatus);
	});

	return false;
}

function createTown(nameTown, done) {
	var obj = new Object();
	obj.Name = nameTown;
	var json = $.toJSON(obj);

	var request = $.ajax({
		type: 'POST',
		url: '//' + window.location.host + '/Town/CreateAJAX',
		data: json,
		contentType: 'application/json; charset=utf-8'
	});

	request.done(done);

	request.fail(function (jqXHR, textStatus) {
		alert("Request failed: " + textStatus);
	});

	return false;
}

/*
* Делает поле с классом class_creater, при изменении
* его содержание добавляется в блок с классом inner_conteiner
* Применяется для тех элементов которые помещены в блок с ownerId	
*
* @param {string} class_creater элемент при изменении которого, добавляется новый элемент в inner_conteiner
* @param {string} inner_conteiner Контейнер куда дабовлять новые элементы
* @param {string} ownerId Внешний id.
*/
function createAutoAddedRow(class_creater, inner_conteiner, ownerId) {

	var selectorId = '#' + ownerId + ' ';

	$(selectorId + '.' + class_creater).change(function () {

		var creater = $(selectorId + '.' + class_creater);
		var creater_value = creater.val();

		var inputWithButton = document.createElement('div');
		inputWithButton.classList.add('input-group');

		var ipt = document.createElement('input');
		ipt.type = 'text';
		ipt.classList.add('form-control');
		ipt.value = creater_value;

		var spn = document.createElement('span');
		spn.classList.add('input-group-btn');

		var btn = document.createElement('button');
		btn.classList.add('btn');
		btn.classList.add('btn-default');
		btn.type = 'button';
		btn.onclick = function () { $(inputWithButton).remove(); }

		var innSpn = document.createElement('span');
		innSpn.classList.add('glyphicon');
		innSpn.classList.add('glyphicon-remove');

		btn.appendChild(innSpn);
		spn.appendChild(btn);

		inputWithButton.appendChild(ipt);
		inputWithButton.appendChild(spn);

		$(inputWithButton).appendTo(selectorId + '.' + inner_conteiner);

		creater.val("");
	});
	return false;
}

//[TODO]: Временная заглушка для обработки если сервер добавляет рекламу
function fixJSON(stringJSON) {
	//var rezult = stringJSON.split('<!--');
	//return rezult[0];

	return stringJSON;
}