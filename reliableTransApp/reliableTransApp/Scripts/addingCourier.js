﻿/// <reference path="../bootstrap.js" />
/// <reference path="reliableTrans.js" />
/// <reference path="jquery-1.9.1.js" />

/**
 * Скрипты для обработки формы Create, для формы ЭкспрессРБ
 *
 * @author Igor Nesterenya
 * @version 0.01
 */
$(function () {

	//Вызов функции инициализации
	main();

});

function main() {
	setRusianCalender();
	configureAutoaddedContactFilds("contacts");
}

function castContactDataToSend() {
	var hiddenContacts = $("#hidden-contacts");
	hiddenContacts.empty();

	var contact = formContactData("contacts");
	//Заполняем скрытые поля для отправки на сервер контактов
	for (var i = 0 ; i < contact.PhoneNumbers.length; i++) {
		var name = "PhoneNumbers["+i.toString()+"].Phone";
		var hidden_fild = formHiddenFild(name, contact.PhoneNumbers[i].Phone);
			
		//TODO: можно оптимизировать
		hiddenContacts.append($(hidden_fild));
	}

	for (var i = 0 ; i < contact.Emailes.length; i++) {
		var name = "Emailes[" + i.toString() + "].Address";
		var hidden_fild = formHiddenFild(name, contact.Emailes[i].Address);

		hiddenContacts.append($(hidden_fild));
	}
	
	for (var i = 0 ; i < contact.Faxes.length; i++) {
		var name = "Faxes[" + i.toString() + "].Number";
		var hidden_fild = formHiddenFild(name, contact.Faxes[i].Number);

		hiddenContacts.append($(hidden_fild));
	}

}

function formHiddenFild(name, value) {
	var hidden_fild = document.createElement("input");
	hidden_fild.type="hidden";
	hidden_fild.value = value;
	hidden_fild.name = name;

	return hidden_fild;
}