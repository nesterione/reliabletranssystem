﻿/// <reference path="../bootstrap.js" />
/// <reference path="reliableTrans.js" />
/// <reference path="jquery-1.9.1.js" />

/**
 * Скрипты для обработки формы Create, для формы ЭкспрессРБ
 *
 * @author Igor Nesterenya
 * @version 0.01
 */
$(function () {
	//Вызов функции инициализации
	main();
});

function main() {
	setRusianCalender();
}