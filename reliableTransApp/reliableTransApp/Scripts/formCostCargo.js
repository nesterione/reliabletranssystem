﻿/// <reference path="../Views/Scripts/jquery-1.8.2.js" />

function calcCost() {
	var AVEREGE_DENSITY = 150; //кг на метр кубический

	try {

		var totalWeight = calcTotalWeight();
		var totalVolume = calcTotalVolume();

		var biggerValue = ((AVEREGE_DENSITY * totalVolume) < totalWeight) ? totalWeight : (AVEREGE_DENSITY * totalVolume);

		var tarif = getTarif();
		
		var cost = tarif * biggerValue;
		return cost;

	} catch (e) {
		alert(e.message);
		return -1;
	}
}

function getTarif() {
	var xmlhttp = getXmlHttp();
	xmlhttp.open('POST', '//' + window.location.host + '/Tariffs/GetStandartRate', false);
	xmlhttp.send(null);
	if (xmlhttp.status == 200) {
		var result = parseFloat(xmlhttp.responseText);
		return result;
	}
	else {
		throw {
			message: "Не удалось получить тариф с сервера",
			code: 1101
		}
	}
}

function getXmlHttp() {
	var xmlhttp;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
		}
	}
	if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}

function calcTotalWeight() {
	var totalWeight = 0;
	$("#CargoesTable tbody tr").each(function () {
	  var weightOne =	$(this).find("[name*='Weight']").val();
	  var count = $(this).find("[name*='Count']").val();
	  totalWeight += weightOne * count;
	});

	if ((typeof(totalWeight)=="number")&& totalWeight > 0) {
		return totalWeight;
	}
	else {
		throw {
			message: "Не правильные данные",
			code: 1101
		}
	}
}

function calcTotalVolume() {
	var totalVolume = 0;
	$("#CargoesTable tbody tr").each(function () {
		var cargoHeight = $(this).find("[name*='Height']").val();
		var cargoWidth = $(this).find("[name*='Width']").val();
		var cargoLenght = $(this).find("[name*='Lenght']").val();

		var count = $(this).find("[name*='Count']").val();
		totalVolume += (cargoHeight*cargoLenght*cargoWidth*count);
	});

	if ((typeof (totalVolume) == "number") && totalVolume > 0) {
		return totalVolume;
	}
	else {
		throw {
			message: "Не правильные данные",
			code: 1101
		}
	}
}

function setCost() {
	if ($("#standart-rate").hasClass("active")) {
		var cost = calcCost();
		if (cost > 0) {
			$("#Price").val(cost);
		} else {
			alert("Стоимость не посчитана");
		}
	}
	else {
		alert("Расчет стоимости может применяться только при выборе основного тарифа");
	}

}