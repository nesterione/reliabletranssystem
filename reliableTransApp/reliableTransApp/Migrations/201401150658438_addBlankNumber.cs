namespace reliableTransApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addBlankNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExpressRBs", "BlankNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExpressRBs", "BlankNumber");
        }
    }
}
