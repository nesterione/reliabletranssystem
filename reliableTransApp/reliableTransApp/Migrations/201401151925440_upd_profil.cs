namespace reliableTransApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class upd_profil : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "PassportDataId", c => c.Guid());
            AddColumn("dbo.UserProfile", "ContactId", c => c.Guid());
            AddForeignKey("dbo.UserProfile", "PassportDataId", "dbo.PassportDatas", "Id");
            AddForeignKey("dbo.UserProfile", "ContactId", "dbo.Contacts", "Id");
            CreateIndex("dbo.UserProfile", "PassportDataId");
            CreateIndex("dbo.UserProfile", "ContactId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserProfile", new[] { "ContactId" });
            DropIndex("dbo.UserProfile", new[] { "PassportDataId" });
            DropForeignKey("dbo.UserProfile", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.UserProfile", "PassportDataId", "dbo.PassportDatas");
            DropColumn("dbo.UserProfile", "ContactId");
            DropColumn("dbo.UserProfile", "PassportDataId");
        }
    }
}
