namespace reliableTransApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addEnumInDb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExpressRBs", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExpressRBs", "Status");
        }
    }
}
