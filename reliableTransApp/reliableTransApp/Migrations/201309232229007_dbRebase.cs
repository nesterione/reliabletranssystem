namespace reliableTransApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dbRebase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Towns",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ExpressRBs",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DateLoading = c.DateTime(),
                        DateUnloading = c.DateTime(),
                        SenderId = c.Guid(nullable: false),
                        RecipientId = c.Guid(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.SenderId)
                .ForeignKey("dbo.Customers", t => t.RecipientId)
                .Index(t => t.SenderId)
                .Index(t => t.RecipientId);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PhoneNumbers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Phone = c.String(nullable: false, maxLength: 50),
                        ContactId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .Index(t => t.ContactId);
            
            CreateTable(
                "dbo.Faxes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Number = c.String(nullable: false, maxLength: 50),
                        ContactId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .Index(t => t.ContactId);
            
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Address = c.String(nullable: false, maxLength: 50),
                        ContactId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .Index(t => t.ContactId);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Note = c.String(nullable: false, maxLength: 200),
                        ContactId = c.Guid(nullable: false),
                        Name = c.String(),
                        Surname = c.String(),
                        Patronymic = c.String(),
                        PassportDataId = c.Guid(nullable: false),
                        AddressId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .ForeignKey("dbo.PassportDatas", t => t.PassportDataId, cascadeDelete: true)
                .ForeignKey("dbo.Addresses", t => t.AddressId, cascadeDelete: true)
                .Index(t => t.ContactId)
                .Index(t => t.PassportDataId)
                .Index(t => t.AddressId);
            
            CreateTable(
                "dbo.PassportDatas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Series = c.String(nullable: false, maxLength: 5),
                        Number = c.Int(nullable: false),
                        Issued = c.DateTime(nullable: false),
                        IssuedBy = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TownId = c.Guid(nullable: false),
                        Street = c.String(nullable: false, maxLength: 200),
                        Number = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Towns", t => t.TownId, cascadeDelete: true)
                .Index(t => t.TownId);
            
            CreateTable(
                "dbo.Cargoes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Lenght = c.Double(nullable: false),
                        Width = c.Double(nullable: false),
                        Height = c.Double(nullable: false),
                        Weight = c.Double(nullable: false),
                        ExpressRBId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ExpressRBs", t => t.ExpressRBId)
                .Index(t => t.ExpressRBId);
            
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Cargoes", new[] { "ExpressRBId" });
            DropIndex("dbo.Addresses", new[] { "TownId" });
            DropIndex("dbo.Customers", new[] { "AddressId" });
            DropIndex("dbo.Customers", new[] { "PassportDataId" });
            DropIndex("dbo.Customers", new[] { "ContactId" });
            DropIndex("dbo.Emails", new[] { "ContactId" });
            DropIndex("dbo.Faxes", new[] { "ContactId" });
            DropIndex("dbo.PhoneNumbers", new[] { "ContactId" });
            DropIndex("dbo.ExpressRBs", new[] { "RecipientId" });
            DropIndex("dbo.ExpressRBs", new[] { "SenderId" });
            DropForeignKey("dbo.Cargoes", "ExpressRBId", "dbo.ExpressRBs");
            DropForeignKey("dbo.Addresses", "TownId", "dbo.Towns");
            DropForeignKey("dbo.Customers", "AddressId", "dbo.Addresses");
            DropForeignKey("dbo.Customers", "PassportDataId", "dbo.PassportDatas");
            DropForeignKey("dbo.Customers", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.Emails", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.Faxes", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.PhoneNumbers", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.ExpressRBs", "RecipientId", "dbo.Customers");
            DropForeignKey("dbo.ExpressRBs", "SenderId", "dbo.Customers");
            DropTable("dbo.UserProfile");
            DropTable("dbo.Cargoes");
            DropTable("dbo.Addresses");
            DropTable("dbo.PassportDatas");
            DropTable("dbo.Customers");
            DropTable("dbo.Emails");
            DropTable("dbo.Faxes");
            DropTable("dbo.PhoneNumbers");
            DropTable("dbo.Contacts");
            DropTable("dbo.ExpressRBs");
            DropTable("dbo.Towns");
        }
    }
}
