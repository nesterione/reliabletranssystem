namespace reliableTransApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addLastMigragionErrorsFix : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Couriers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FirstName = c.String(),
                        SurName = c.String(),
                        Patronymic = c.String(),
                        PassportDataId = c.Guid(nullable: false),
                        AddressId = c.Guid(nullable: false),
                        ContactId = c.Guid(nullable: false),
                        DateIssueWaybill = c.DateTime(),
                        PaymentTypeId = c.Guid(),
                        CarId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PassportDatas", t => t.PassportDataId, cascadeDelete: true)
                .ForeignKey("dbo.Addresses", t => t.AddressId, cascadeDelete: true)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .ForeignKey("dbo.PaymentTypes", t => t.PaymentTypeId)
                .ForeignKey("dbo.Cars", t => t.CarId)
                .Index(t => t.PassportDataId)
                .Index(t => t.AddressId)
                .Index(t => t.ContactId)
                .Index(t => t.PaymentTypeId)
                .Index(t => t.CarId);
            
            CreateTable(
                "dbo.PaymentTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CarBrand = c.String(),
                        LicensePlate = c.String(),
                        CubicCapacity = c.Double(nullable: false),
                        WeightCapacity = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Couriers", new[] { "CarId" });
            DropIndex("dbo.Couriers", new[] { "PaymentTypeId" });
            DropIndex("dbo.Couriers", new[] { "ContactId" });
            DropIndex("dbo.Couriers", new[] { "AddressId" });
            DropIndex("dbo.Couriers", new[] { "PassportDataId" });
            DropForeignKey("dbo.Couriers", "CarId", "dbo.Cars");
            DropForeignKey("dbo.Couriers", "PaymentTypeId", "dbo.PaymentTypes");
            DropForeignKey("dbo.Couriers", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.Couriers", "AddressId", "dbo.Addresses");
            DropForeignKey("dbo.Couriers", "PassportDataId", "dbo.PassportDatas");
            DropTable("dbo.Cars");
            DropTable("dbo.PaymentTypes");
            DropTable("dbo.Couriers");
        }
    }
}
