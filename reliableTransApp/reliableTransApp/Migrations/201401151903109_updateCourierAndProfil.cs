namespace reliableTransApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateCourierAndProfil : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Couriers", "PassportDataId", "dbo.PassportDatas");
            DropForeignKey("dbo.Couriers", "AddressId", "dbo.Addresses");
            DropForeignKey("dbo.Couriers", "ContactId", "dbo.Contacts");
            DropIndex("dbo.Couriers", new[] { "PassportDataId" });
            DropIndex("dbo.Couriers", new[] { "AddressId" });
            DropIndex("dbo.Couriers", new[] { "ContactId" });
            AddColumn("dbo.Couriers", "UserProfileId", c => c.Guid());
            AddColumn("dbo.Couriers", "UserProfile_UserId", c => c.Int());
            AddColumn("dbo.UserProfile", "Address", c => c.String());
            AddForeignKey("dbo.Couriers", "UserProfile_UserId", "dbo.UserProfile", "UserId");
            CreateIndex("dbo.Couriers", "UserProfile_UserId");
            DropColumn("dbo.Couriers", "FirstName");
            DropColumn("dbo.Couriers", "SurName");
            DropColumn("dbo.Couriers", "Patronymic");
            DropColumn("dbo.Couriers", "PassportDataId");
            DropColumn("dbo.Couriers", "AddressId");
            DropColumn("dbo.Couriers", "ContactId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Couriers", "ContactId", c => c.Guid(nullable: false));
            AddColumn("dbo.Couriers", "AddressId", c => c.Guid(nullable: false));
            AddColumn("dbo.Couriers", "PassportDataId", c => c.Guid(nullable: false));
            AddColumn("dbo.Couriers", "Patronymic", c => c.String());
            AddColumn("dbo.Couriers", "SurName", c => c.String());
            AddColumn("dbo.Couriers", "FirstName", c => c.String());
            DropIndex("dbo.Couriers", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.Couriers", "UserProfile_UserId", "dbo.UserProfile");
            DropColumn("dbo.UserProfile", "Address");
            DropColumn("dbo.Couriers", "UserProfile_UserId");
            DropColumn("dbo.Couriers", "UserProfileId");
            CreateIndex("dbo.Couriers", "ContactId");
            CreateIndex("dbo.Couriers", "AddressId");
            CreateIndex("dbo.Couriers", "PassportDataId");
            AddForeignKey("dbo.Couriers", "ContactId", "dbo.Contacts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Couriers", "AddressId", "dbo.Addresses", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Couriers", "PassportDataId", "dbo.PassportDatas", "Id", cascadeDelete: true);
        }
    }
}
