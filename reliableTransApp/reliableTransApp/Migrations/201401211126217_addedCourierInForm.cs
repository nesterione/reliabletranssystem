namespace reliableTransApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedCourierInForm : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExpressRBs", "CourierOneId", c => c.Guid());
            AddColumn("dbo.ExpressRBs", "CourierTwoId", c => c.Guid());
            AddForeignKey("dbo.ExpressRBs", "CourierOneId", "dbo.Couriers", "Id");
            AddForeignKey("dbo.ExpressRBs", "CourierTwoId", "dbo.Couriers", "Id");
            CreateIndex("dbo.ExpressRBs", "CourierOneId");
            CreateIndex("dbo.ExpressRBs", "CourierTwoId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ExpressRBs", new[] { "CourierTwoId" });
            DropIndex("dbo.ExpressRBs", new[] { "CourierOneId" });
            DropForeignKey("dbo.ExpressRBs", "CourierTwoId", "dbo.Couriers");
            DropForeignKey("dbo.ExpressRBs", "CourierOneId", "dbo.Couriers");
            DropColumn("dbo.ExpressRBs", "CourierTwoId");
            DropColumn("dbo.ExpressRBs", "CourierOneId");
        }
    }
}
