namespace reliableTransApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTariff : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tariffs",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Value = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Tariffs");
        }
    }
}
