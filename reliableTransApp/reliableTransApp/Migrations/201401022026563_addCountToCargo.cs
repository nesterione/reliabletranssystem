namespace reliableTransApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCountToCargo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cargoes", "Count", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cargoes", "Count");
        }
    }
}
