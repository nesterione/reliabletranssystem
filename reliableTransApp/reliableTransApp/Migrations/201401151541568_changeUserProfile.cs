namespace reliableTransApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeUserProfile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "Name", c => c.String());
            AddColumn("dbo.UserProfile", "Surname", c => c.String());
            AddColumn("dbo.UserProfile", "Patronymic", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfile", "Patronymic");
            DropColumn("dbo.UserProfile", "Surname");
            DropColumn("dbo.UserProfile", "Name");
        }
    }
}
