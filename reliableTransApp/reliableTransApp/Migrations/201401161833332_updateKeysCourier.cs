namespace reliableTransApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateKeysCourier : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Couriers", name: "UserProfile_UserId", newName: "UserId");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.Couriers", name: "UserId", newName: "UserProfile_UserId");
        }
    }
}
