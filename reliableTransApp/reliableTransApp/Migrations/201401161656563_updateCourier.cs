namespace reliableTransApp.Migrations
{
  using reliableTransApp.Models.DataBaseContext;
  using System;
  using System.Data.Entity.Migrations;
    
    public partial class updateCourier : DbMigration
    {
        public override void Up()
        {
          DropColumn("dbo.Couriers", "UserProfileId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Couriers", "UserProfileId", c => c.Guid());
        }
    }
}
